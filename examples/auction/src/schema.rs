// Copyright 2018 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! The database schema.

use exonum::{
    crypto::{Hash, PublicKey}, storage::{Fork, ProofListIndex, ProofMapIndex, Snapshot},
};

use lot::Lot;

#[derive(Debug)]
pub struct Schema<T> {
    view: T,
}

impl<T> AsMut<T> for Schema<T> {
    fn as_mut(&mut self) -> &mut T {
        &mut self.view
    }
}

impl<T> Schema<T>
where
    T: AsRef<dyn Snapshot>,
{
    /// Creates a new schema from the database view.
    pub fn new(view: T) -> Self {
        Schema { view }
    }

    /// Returns `ProofMapIndex` with auction lots.
    pub fn lots(&self) -> ProofMapIndex<&T, PublicKey, Lot> {
        ProofMapIndex::new("auction.lots", &self.view)
    }

    /// Returns history of the lot with the given public key.
    pub fn lot_history(&self, public_key: &PublicKey) -> ProofListIndex<&T, Hash> {
        ProofListIndex::new_in_family("auction.lot_history", public_key, &self.view)
    }

    /// Returns the lot corresponding to the given public key.
    pub fn lot(&self, pub_key: &PublicKey) -> Option<Lot> {
        self.lots().get(pub_key)
    }

    /// Returns the state hash of cryptocurrency service.
    pub fn state_hash(&self) -> Vec<Hash> {
        vec![self.lots().merkle_root()]
    }
}

/// Implementation of mutable methods.
impl<'a> Schema<&'a mut Fork> {
    /// Returns mutable `ProofMapIndex` with lots.
    pub fn lots_mut(&mut self) -> ProofMapIndex<&mut Fork, PublicKey, Lot> {
        ProofMapIndex::new("auction.lots", &mut self.view)
    }

    /// Returns history for the lot by the given public key.
    pub fn lot_history_mut(
        &mut self,
        public_key: &PublicKey,
    ) -> ProofListIndex<&mut Fork, Hash> {
        ProofListIndex::new_in_family("auction.lot_history", public_key, &mut self.view)
    }

    /// Register a new best bid.
    ///
    /// Panics if there is no lot with given public key.
    pub fn new_best_bid(&mut self, lot: Lot, amount: u64, bidder: &PublicKey, transaction: &Hash) {
        let lot = {
            let mut history = self.lot_history_mut(lot.pub_key());
            history.push(*transaction);
            let history_hash = history.merkle_root();
            lot.update_bid(amount, bidder, &history_hash)
        };
        self.lots_mut().put(lot.pub_key(), lot.clone());
    }

    /// Create a new lot with the first bid coming from its owner.
    pub fn create_lot(&mut self, key: &PublicKey, description: &str, starting_price: u64, transaction: &Hash) {
        let lot = {
            let mut history = self.lot_history_mut(key);
            history.push(*transaction);
            let history_hash = history.merkle_root();
            Lot::new(key, description, starting_price, key, history.len(), &history_hash)
        };
        self.lots_mut().put(key, lot);
    }
}
