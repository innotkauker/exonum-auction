// Copyright 2018 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! An auction lot taking cryptocurrency bids.

use exonum::crypto::{Hash, PublicKey};

encoding_struct! {
    /// Stored in the database. Pub_key is the public key of the user who created the wallet,
    /// and it also refers to a wallet which is used to hold the last bid.
    /// Bidder holds the key of the wallet that made the best bid to this moment.
    struct Lot {
        pub_key:            &PublicKey,
        description:        &str,
        best_bid:           u64,
        bidder:             &PublicKey,
        history_len:        u64,
        history_hash:       &Hash,
    }
}

impl Lot {
    // Registers the next best bid in a new object.
    pub fn update_bid(self, bid: u64, bidder: &PublicKey, history_hash: &Hash) -> Self {
        Self::new(
            self.pub_key(),
            self.description(),
            bid,
            bidder,
            self.history_len() + 1,
            history_hash,
        )
    }
}
