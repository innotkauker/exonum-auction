// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Cryptocurrency transactions.

// Workaround for `failure` see https://github.com/rust-lang-nursery/failure/issues/223 and
// ECR-1771 for the details.
#![allow(bare_trait_objects)]

use exonum::{
    blockchain::{ExecutionError, ExecutionResult, Transaction}, crypto::{CryptoHash, PublicKey},
    messages::Message, storage::Fork,
};

use schema::Schema as AuctionSchema;
use AUCTION_SERVICE_ID;

extern crate exonum_cryptocurrency_advanced as cryptocurrency;
use self::cryptocurrency::schema::Schema as CurrencySchema;

/// Error codes emitted by wallet transactions during execution.
#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    #[fail(display = "Lot already exists")]
    LotAlreadyExists,

    #[fail(display = "Not the best bid")]
    NotTheBestBid,

    /// Sender doesn't exist.
    ///
    /// Can be emitted by `Bid`.
    #[fail(display = "Bidder doesn't exist")]
    BidderNotFound,

    /// Lot doesn't exist.
    ///
    /// Can be emitted by `Bid`.
    #[fail(display = "Lot doesn't exist")]
    LotNotFound,

    /// Insufficient currency amount.
    ///
    /// Can be emitted by `Bid`.
    #[fail(display = "Insufficient currency amount")]
    InsufficientCurrencyAmount,

    /// Previous bidder not found.
    ///
    /// Can be emitted by `Bid`.
    /// With current functionality, this error indicates a problem with implementation logic,
    /// since the owner doesn't get refunded, other wallets have existed at
    /// the point of bidding and there is no functionality to remove wallets.
    #[fail(display = "Previous bidder not found")]
    PreviousBidderNotFound,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}

transactions! {
    /// Transaction group.
    pub LotTransactions {
        const SERVICE_ID = AUCTION_SERVICE_ID;

        /// Make a bid from the given account. The amount bidded is taken from the bidder's
        /// account. The amount of the previous bid is refunded onto the account of the previous
        /// bidder.
        ///
        /// There is no need to prevent this transaction from being replayed (i.e. to include a
        /// seed) since to update the price one has to bid higher. Replayed bids won't get into the
        /// blockchain.
        struct Bid {
            /// `PublicKey` of sender's wallet.
            bidder:    &PublicKey,
            /// `PublicKey` of the lot.
            lot:   &PublicKey,
            /// Updated price value.
            amount:  u64,
        }

        /// Create wallet with the given `name`.
        struct CreateLot {
            /// `PublicKey` of the lot.
            pub_key: &PublicKey,
            /// Name of the lot.
            description: &str,
            starting_price: u64,
        }
    }
}

impl Transaction for Bid {
    fn verify(&self) -> bool {
        (self.bidder() != self.lot()) // Don't let the owner make new bids
            && self.verify_signature(self.bidder())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let from = self.bidder();
        let to = self.lot();
        let hash = self.hash();
        let amount = self.amount();

        let refund = {
            let mut auction_schema = AuctionSchema::new(&mut *fork);
            let lot = auction_schema.lot(to).ok_or(Error::LotNotFound)?;

            if lot.best_bid() >= amount {
                Err(Error::NotTheBestBid)?
            }
            let last_bidder = *lot.bidder();
            let lot_creator = *lot.pub_key();
            let last_bid = lot.best_bid();
            auction_schema.new_best_bid(lot, amount, from, &hash);
            if last_bidder == lot_creator {
                // We don't refund the initial price to the creator.
                None
            } else {
                Some((lot_creator, last_bid))
            }
        };

        let mut currency_schema = CurrencySchema::new(fork);
        let bidder = currency_schema.wallet(from).ok_or(Error::BidderNotFound)?;

        if bidder.balance() < amount {
            Err(Error::InsufficientCurrencyAmount)?
        }
        currency_schema.decrease_wallet_balance(bidder, amount, &hash);

        match refund {
            Some((bidder, best_bid)) => {
                let previous_bidder = currency_schema.wallet(&bidder).ok_or(Error::PreviousBidderNotFound)?;
                currency_schema.increase_wallet_balance(previous_bidder, best_bid, &hash);
            },
            None => ()
        }

        Ok(())
    }
}

impl Transaction for CreateLot {
    fn verify(&self) -> bool {
        self.verify_signature(self.pub_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = AuctionSchema::new(fork);
        let pub_key = self.pub_key();
        let hash = self.hash();

        if schema.lot(pub_key).is_none() {
            schema.create_lot(pub_key, self.description(), self.starting_price(), &hash);
            Ok(())
        } else {
            Err(Error::LotAlreadyExists)?
        }
    }
}
