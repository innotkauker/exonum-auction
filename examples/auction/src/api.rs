// Copyright 2018 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Auction API.

use exonum::{
    api::{self, ServiceApiBuilder, ServiceApiState},
    blockchain::{self, BlockProof, Transaction}, crypto::{Hash, PublicKey},
    helpers::Height, node::TransactionSend, storage::{MapProof},
};

use std::{thread};
use std::sync::{Mutex, Condvar};

use transactions::LotTransactions;
use lot::Lot;
use {Schema, AUCTION_SERVICE_ID};

/// This is used to notify the threads that handle transactions about new blocks.
/// It could be stored in the Service (which has a handle to catch these notifications from the
/// blockchain), but since there doesn't seem to be a way to access our Service's special methods
/// without having a static reference to it, why not just have a reference to the condition var
/// itself.
#[derive(Debug)]
pub struct BlockNotification {
    lock: Mutex<u64>,
    cvar: Condvar,
}

impl BlockNotification {
    pub fn new() -> Self {
        let lock = Mutex::new(0u64);
        let cvar = Condvar::new();
        BlockNotification { lock, cvar }
    }
    pub fn new_block(&self, height: u64) {
        let mut mutex_guard = self.lock.lock().unwrap();
        *mutex_guard = height;
        self.cvar.notify_all(); // wake the transaction handlers waiting for new blocks.
    }
    pub fn wait_for_new_block(&self) -> u64 {
        let mut mutex_guard = self.lock.lock().unwrap();
        let prev_block_id = *mutex_guard;
        // Using < since we don't initialize the block number in the mutex before generating
        // the first block - a transaction might be posted before that.
        while *mutex_guard <= prev_block_id {
            mutex_guard = self.cvar.wait(mutex_guard).unwrap();
        }
        *mutex_guard
    }
}

lazy_static! {
    pub static ref BLOCK_NOTIFICATIONS: BlockNotification = BlockNotification::new();
}

/// Response to an incoming transaction returned by the REST API.
#[derive(Debug, Serialize, Deserialize)]
pub struct TransactionResponse {
    pub tx_hash: Hash,
    pub block_id: u64,
}

/// Describes the query parameters for the `get_lot` endpoint.
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct LotQuery {
    /// Public key of the queried lot.
    pub pub_key: PublicKey,
}

/// Proof of existence for specific lot.
#[derive(Debug, Serialize, Deserialize)]
pub struct LotProof {
    /// Proof of the whole database table.
    pub to_table: MapProof<Hash, Hash>,
    /// Proof of the specific lot in this table.
    pub to_lot: MapProof<PublicKey, Lot>,
}

/// Lot information.
#[derive(Debug, Serialize, Deserialize)]
pub struct LotInfo {
    /// Proof of the last block.
    pub block_proof: BlockProof,
    /// Proof of the appropriate lot.
    pub lot_proof: LotProof,
    pub best_bid: u64,
}

/// Public service API description.
#[derive(Debug, Clone, Copy)]
pub struct PublicApi;

impl PublicApi {
    /// Endpoint for getting a single lot.
    pub fn lot_info(state: &ServiceApiState, query: LotQuery) -> api::Result<LotInfo> {
        let snapshot = state.snapshot();
        let general_schema = blockchain::Schema::new(&snapshot);
        let auction_schema = Schema::new(&snapshot);

        let max_height = general_schema.block_hashes_by_height().len() - 1;

        let block_proof = general_schema
            .block_and_precommits(Height(max_height))
            .unwrap();

        let to_table: MapProof<Hash, Hash> =
            general_schema.get_proof_to_service_table(AUCTION_SERVICE_ID, 0);

        let to_lot: MapProof<PublicKey, Lot> =
            auction_schema.lots().get_proof(query.pub_key);

        let lot_proof = LotProof {
            to_table,
            to_lot,
        };

        let lot = auction_schema.lot(&query.pub_key).ok_or(api::Error::NotFound("Lot not found".to_owned()))?;
        let best_bid = lot.best_bid();

        Ok(LotInfo {
            block_proof,
            lot_proof,
            best_bid,
        })
    }

    /// Endpoint for handling transactions.
    pub fn post_transaction(
        state: &ServiceApiState,
        query: LotTransactions,
    ) -> api::Result<TransactionResponse> {
        println!("post_transaction in thread #{:?}", thread::current().id());
        let transaction: Box<dyn Transaction> = query.into();
        let tx_hash = transaction.hash();
        state.sender().send(transaction)?;
        let block_id = BLOCK_NOTIFICATIONS.wait_for_new_block();
        Ok(TransactionResponse{ tx_hash, block_id })
    }

    /// Wires the above endpoint to public scope of the given `ServiceApiBuilder`.
    pub fn wire(builder: &mut ServiceApiBuilder) {
        println!("wire in thread #{:?}", thread::current().id());
        builder.public_scope()
               .endpoint("v1/lots/info", Self::lot_info)
               .endpoint_mut("v1/lots/transaction", Self::post_transaction);
    }
}
